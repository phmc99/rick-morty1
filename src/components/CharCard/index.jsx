import "./style.css";

export const CharCard = ({ name, image, status }) => {
  return (
    <div className={status + " oi"}>
      <h1>{name}</h1>
      <img src={image} alt={name} />
    </div>
  );
};
